# Project Name

> Book Store

### Description

> A Frontend Vue.js app that consumes Google Books API

## Project setup

- Clone this repository, cd into it
- Make dure you have Node.js installed, then run `npm install` to download and install project dependencies
- Run the development server by using `npm run serve`
- If you want to build the project for production, you can use `npm run build`.
- If you want to check and fix linting issues, run `npm run lint`.

### Author

> Elie Mugenzi

