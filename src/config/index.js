
const config = {
    BASE_URL: 'https://www.googleapis.com/books/v1/volumes'
};

export default config;
