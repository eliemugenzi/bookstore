import Vue from 'vue';
import VueMeta from 'vue-meta';
import VueCompositionAPI from '@vue/composition-api';
import VueSax from 'vuesax';
import '@fortawesome/fontawesome-free/css/all.min.css';
import VueLazyLoad from 'vue-lazyload';
import moment from 'moment';

import App from './App.vue';
import router from './routes';
import store from './store';

import 'vuesax/dist/vuesax.css';


Vue.config.productionTip = false;

Vue.use(VueCompositionAPI);
Vue.use(VueSax);
Vue.use(VueMeta);
Vue.use(VueLazyLoad);

Vue.filter('format_date', (value) => {
  if(value){
    return moment(String(value)).format('LL');
  }
});

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app');
