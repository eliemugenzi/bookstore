

const state = {
    books: [],
};

const mutations = {
    GET_BOOKS: (state, payload)=>{
        state.books=payload;
    }
};

const getters = {
    getBooks: state=>state.books,
};

export default {
    state,
    mutations,
    getters,
}
