import { toRefs, reactive } from '@vue/composition-api';
import store from '../store';

const useFetch = (url, options, { action }) =>{
    const state = reactive({
        response: [],
        error: null,
        fetching: false,
        fetched: false,
    });

    const fetchData = async()=>{
        state.fetching=true;

        try {
            const res = await fetch(url, options);
            if(res.status!==200){
                state.error = 'Something went wrong!';
            } else {
                const jsonResponse = await res.json();
                state.response = jsonResponse;

                if(action==='GET_BOOKS'){
                  store.commit('GET_BOOKS', state.response.items);
                }
            }
            
        } catch(error){
            state.error = error;
        } finally {
            state.fetching = false;
            state.fetched = true;
        }
    };

    return {
        ...toRefs(state),
        fetchData,
    };
}

export default useFetch;
