import { toRefs, reactive, ref } from '@vue/composition-api';
import useFetch from './useFetch';
import config from '../config';

const getBooks = ()=> {
   let books = reactive({
       list: [],
       error: null,
       fetching: false,
       fetched: false,
   });

   const val = ref('');

   const submitted = async ()=>{
      const { error, response, fetchData, fetching, fetched } = await useFetch(`${config.BASE_URL}?q=${val.value}`, {}, {
          action: 'GET_BOOKS'
      });

      fetchData();


      books.list = response;
      books.error = error;
      books.fetching = fetching;
      books.fetched = fetched;
   }

   return {
       ...toRefs(books),
       submitted,
       val,
   }
};

export default getBooks;
