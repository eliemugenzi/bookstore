import { toRefs, reactive } from '@vue/composition-api';
import useFetch from './useFetch';
import config from '../config';

const getSingleBook = ({_id}) =>{
    let book = reactive({
        item: null,
        error: null,
        fetching: null,
    });


    const getBook = async ()=>{
       const { fetchData, fetching, response, error } = await useFetch(`${config.BASE_URL}/${_id}`, {}, { action: 'GET_BOOK'});

       fetchData();

       book.fetching=fetching;
       book.error = error;
       book.item=response;
    }

    return {
        ...toRefs(book),
        getBook,
    }
}

export default getSingleBook;
