import VueRouter from 'vue-router';
import Vue from 'vue';
import Home from '../views/index.vue';
import SingleBook from '../views/SingleBook.vue';
import NotFound from '../views/404.vue';

const routes = [
    {
        path: '/',
        component: Home,
    },
    {
        path: '/books/:id',
        component: SingleBook,
    },
    {
        path: "*",
        component: NotFound,
    }
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

Vue.use(VueRouter);

export default router;
